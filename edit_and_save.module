<?php

/**
 * @file
 * This module is about adding edit and save button.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\edit_and_save\Repository\EditAndSaveRepository;

/**
 * Implements hook_form_alter().
 */
function edit_and_save_form_alter(array &$form, FormStateInterface $form_state, $form_id) {

  $account = \Drupal::currentUser();
  $config = \Drupal::config('edit_and_save.settings');

  $allowed_forms = EditAndSaveRepository::getAllowedForms();

  if (in_array($form_id, $allowed_forms) && $account->hasPermission('use edit and save')) {
    $form['actions']['edit_and_save'] = $form['actions']['submit'];

    // Overwriting the values from the configuration settings.
    $form['actions']['edit_and_save']['#value'] = $config->get('edit_and_save_button_value');
    $form['actions']['edit_and_save']['#weight'] = $config->get('edit_and_save_button_weight');
    $form['actions']['edit_and_save']['#access'] = TRUE;

    // Adding the submit handler that will redirect back to the edit page.
    $form['actions']['edit_and_save']['#submit'][] = 'edit_and_save_redirect_submit';

    // This allows us to modify the default Save button.
    $form['actions']['submit']['#value'] = $config->get('edit_and_save_default_save_button_value');
    $form['actions']['submit']['#weight'] = $config->get('edit_and_save_default_save_button_weight');

    // If selected from configuration, hide the default save button.
    if ($config->get('edit_and_save_hide_default_save') == 1) {
      $form['actions']['submit']['#access'] = FALSE;
      $form['actions']['submit']['#access'] = FALSE;
    }

    // If selected from configuration, hide the default preview button.
    if ($config->get('edit_and_save_hide_default_preview') == 1) {
      $form['actions']['preview']['#access'] = FALSE;
    }

    // If selected from configuration, hide the default delete button.
    if ($config->get('edit_and_save_hide_default_delete') == 1) {
      $form['actions']['delete']['#access'] = FALSE;
    }

  }
}

/**
 * Custom Submit Handler for Redirecting Save & Edit.
 *
 * @param array $form
 *   The Form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The Form state array.
 */
function edit_and_save_redirect_submit(array $form, FormStateInterface &$form_state) {
  // Checks for "Edit and Save" button.
  if ($form_state->getTriggeringElement()['#id'] == 'edit-edit-and-save') {
    // Removes destination parameter, if it is there.
    \Drupal::request()->query->remove('destination');
    // Gets loaded entity object from the Form state.
    $entity = $form_state->getFormObject()->getEntity();
    $entity_type = $entity->getEntityTypeId();

    $url = Url::fromRoute('entity.' . $entity_type . '.edit_form', [$entity_type => $entity->Id()]);
    // Sets the redirect.
    $form_state->setRedirectUrl($url);
  }
}
