Edit and Save

This is a Drupal 8 port of the https://www.drupal.org/project/save_edit, 
with support for all kinds of
content entities, not only nodes.

Install

Install as a normal module, by going to the extend tab (admin/modules path).

Configure

All the configuration is placed in admin/config/content/edit-save.
There is menu item inside the main configuration screen, under
"Content Authoring section"

Uninstall

Uninstall like every other module, by visiting admin/modules/uninstall
and marking "Edit and Save".
